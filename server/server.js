const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')
const cors = require('cors')


const sequelize = new Sequelize('bratu','root','passw0rd',{
	dialect : 'mysql',
	define : {
		timestamps : false
	}
})

const Subject = sequelize.define('subject', {
	name : Sequelize.STRING,
    long_name : Sequelize.TEXT,
    description : Sequelize.TEXT
})


const Project = sequelize.define('project', {
	name : Sequelize.STRING,
    description : Sequelize.TEXT
})


const Link = sequelize.define('link', {
    url : Sequelize.STRING,
    description : Sequelize.STRING
})


Subject.hasMany(Project)
// Project.belongsTo(Subject)
Project.hasMany(Link)

const app = express()
app.use(cors())
app.use(bodyParser.json())
app.use(express.static('../project-app/build'))

app.get('/create', async (req, res) => {
	try{
		await sequelize.sync({force : true})
		res.status(201).json({message : 'Tables created'})
	}
	catch(error){
		console.warn(error)
		res.status(500).json({message : 'Server error'})
	}
})

app.get('/subjects', async (req, res) => {
	try{
		let subjects = await Subject.findAll()
		res.status(200).json(subjects)
	}
	catch(error){
		console.warn(error)
		res.status(500).json({message : 'Server error'})
	}
})


app.post('/subjects', async (req, res) => {
	try{
        await Subject.create(req.body)
        res.status(201).json({message : 'Subject created'})
	}
	catch(error){
		console.warn(error)
		res.status(500).json({message : 'Server error'})
	}
})

app.get('/subjects/:id', async (req, res) => {
	try{
		let subject = await Subject.findByPk(req.params.id)
		if (subject){
			res.status(200).json(subject)
		}
		else{
			res.status(404).json({message : 'Subject not found'})
		}
	}
	catch(error){
		console.warn(error)
		res.status(500).json({message : 'server error'})
	}
})

app.put('/subjects/:id', async (req, res) => {
	try{
		let subject = await Subject.findByPk(req.params.id)
		if (subject){
			await subject.update(req.body)
			res.status(202).json({message : 'Subject modified'})
		}
		else{
			res.status(404).json({message : 'Subject not found'})
		}
	}
	catch(error){
		console.warn(error)
		res.status(500).json({message : 'Server error'})
	}
})

app.delete('/subjects/:id', async (req, res) => {
	try{
		let subject = await Subject.findByPk(req.params.id)
		if (subject){
			await subject.destroy()
			res.status(202).json({message : 'Subject deleted'})
		}
		else{
			res.status(404).json({message : 'Subject not found'})
		}
	}
	catch(error){
		console.warn(error)
		res.status(500).json({message : 'Server error'})
	}
})

app.get('/subjects/:sid/projects', async (req, res) => {
	try{
		let subject = await Subject.findByPk(req.params.sid)
		if (subject){
			let projects = await subject.getProjects()
			res.status(200).json(projects)
		}
		else{
			res.status(404).json({message : 'Subject not found'})
		}
	}
	catch(error){
		console.warn(error)
		res.status(500).json({message : 'Server error'})
	}
})

app.get('/subjects/:sid/projects/:pid', async (req, res) => {
	try{
		let subject = await Subject.findByPk(req.params.sid)
		if (subject){
			let projects = await subject.getProjects({where : {id : req.params.pid}})
			res.status(200).json(projects.shift())
		}
		else{
			res.status(404).json({message : 'Subject not found'})
		}
	}
	catch(error){
		console.warn(error)
		res.status(500).json({message : 'Server error'})
	}
})

app.post('/subjects/:sid/projects', async (req, res) => {
	try{
		let subject = await Subject.findByPk(req.params.sid)
		if (subject){
			let project = req.body
            project.subjectId = subject.id
            await Project.create(project)
			res.status(201).json({message : 'Project created'})
		}
		else{
			res.status(404).json({message : 'Subject not found'})
		}
	}
	catch(error){
		console.warn(error)
		res.status(500).json({message : 'Server error'})
	}
})

app.put('/subjects/:sid/projects/:pid', async (req, res) => {
	try{
		let subject = await Subject.findByPk(req.params.bid)
		if (subject){
			let projects = await subject.getProjects({where : {id : req.params.pid}})
			let project = projects.shift()
			if (project){
				await project.update(req.body)
				res.status(202).json({message : 'Project modified'})
			}
			else{
				res.status(404).json({message : 'Project not found'})
			}
		}
		else{
			res.status(404).json({message : 'Subject not found'})
		}
	}
	catch(error){
		console.warn(error)
		res.status(500).json({message : 'Server error'})
	}
})

app.delete('/subjects/:sid/projects/:pid', async (req, res) => {
	try{
		let subject = await Subject.findByPk(req.params.sid)
		if (subject){
			let projects = await subject.getProjects({where : {id : req.params.pid}})
			let project = projects.shift()
			if (project){
				await project.destroy(req.body)
				res.status(202).json({message : 'Project deleted'})
			}
			else{
				res.status(404).json({message : 'Project not found not found'})
			}
		}
		else{
			res.status(404).json({message : 'Subject not found'})
		}
	}
	catch(error){
		console.warn(error)
		res.status(500).json({message : 'Server error'})
	}
})


app.get('/subjects/:sid/projects/:pid/links', async (req, res) => {
	try{
		let subject = await Subject.findByPk(req.params.sid)
		if (subject){
			let projects = await subject.getProjects({where : {id : req.params.pid}})
			let project = projects.shift()
			if (project){
                let links = await project.findAll()
			}
			else{
				res.status(404).json({message : 'No links'})
			}
		}
		else{
			res.status(404).json({message : 'Subject not found'})
		}
	}
	catch(error){
		console.warn(error)
		res.status(500).json({message : 'Server error'})
	}
})

app.post('/subjects/:sid/projects/:pid/links', async (req, res) => {
	try{
		let subject = await Subject.findByPk(req.params.sid)
		if (subject){
			let projects = await subject.getProjects({where : {id : req.params.pid}})
            let project = projects.shift()
            if(project){
                let link = req.body
                link.projectId = project.id
                await Link.create(link)
                res.status(201).json({message : 'Link added'})
            }
            else{
                res.status(404).json({message : 'Project not found'})
            }
		}
		else{
			res.status(404).json({message : 'Subject not found'})
		}
	}
	catch(error){
		console.warn(error)
		res.status(500).json({message : 'Server error'})
	}
})


app.put('/subjects/:sid/projects/:pid/links/:lid', async (req, res) => {
    try{
        let subject = await Subject.findByPk(req.params.sid)
        if (subject){
            let projects = await subject.getProjects({where : {id : req.params.pid}})
            let project = projects.shift()
            if(project){
                let links = await Project.getLinks({where : {id : req.params.lid}})
                let link = links.shift()
                if (link){
                    await link.update(req.body)
                    res.status(202).json({message : 'Link modified'})
                }
                else{
                    res.status(404).json({message : 'Link not found'})
                }
            }
            else{
                res.status(404).json({message : 'Project not found'})
            }
        }
        else{
        res.status(404).json({message : 'Subject not found'})
        }
    }
    catch(error){
        console.warn(error)
        res.status(500).json({message : 'Server error'})
    }
})

app.delete('/subjects/:sid/projects/:pid/links/:lid', async (req, res) => {
    try{
        let subject = await Subject.findByPk(req.params.sid)
        if (subject){
            let projects = await subject.getProjects({where : {id : req.params.pid}})
            let project = projects.shift()
            if(project){
                let links = await Project.getLinks({where : {id : req.params.lid}})
                let link = links.shift()
                if (link){
                    await link.destroy()
                    res.status(202).json({message : 'Link removed'})
                }
                else{
                    res.status(404).json({message : 'Link not found'})
                }
            }
            else{
                res.status(404).json({message : 'Project not found'})
            }
        }
        else{
        res.status(404).json({message : 'Subject not found'})
        }
    }
    catch(error){
        console.warn(error)
        res.status(500).json({message : 'Server error'})
    }
})

app.listen(8080)

