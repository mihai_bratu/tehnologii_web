import {EventEmitter} from 'fbemitter';

const SERVER = 'http://localhost:8080'

class ProjectStore {
    constructor(){
        this.projects = [];
        this.emitter = new EventEmitter();
    }

    async getAll(id){
        try{
            let response = await fetch(`${SERVER}/subjects/${id}/projects`);
            let data = await response.json();
            this.projects = data;
            this.emitter.emit("GET_PRJ_SUCC");

        }
        catch(error){
            console.warn("Error while retrieving data from server: "+error);
            this.emitter.emit("GET_PRJ_FAIL");
        }
    }

    async addPrj(subject_id, project){
        try{
            await fetch(`${SERVER}/subjects/${subject_id}`, {
                method : "post",
                headers : {
                    "Content-Type" : "application/json"
                },
                body : JSON.stringify(project)
            })
            this.getAll();
        }
        catch(error){
            console.warn(error);
        }
    }
}

export default ProjectStore;