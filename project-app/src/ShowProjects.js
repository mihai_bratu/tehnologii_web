import React, { Component }  from 'react';

class ShowProjects extends Component{
    constructor(props){
        super(props);
    }

    updateParent(prop, value){
        this.props.update(prop, value);
    }

    render(){
        return <div class="content">
            <button type="button" class="btn btn-success">Adauga proiect</button>
            {
            console.log(this.props.prj)
            }
            {
            console.log(this.props.sbj)
            }
            {
                this.props.prj != null
                ?
                this.props.prj.map((proj, index) => <div key={index}>
                    <h5>{index+1}. {proj.name}</h5>
                    <p>{proj.description}</p>
                    <button type="button" class="btn btn-primary btn-sm">Modifica</button>
                    <button type="button" class="btn btn-secondary btn-sm">Sterge</button>
                </div>)
                : 
                null
            }
            {
                this.props.prj == null
                ?
                <div>
                <p>Niciun proiect</p>
                </div>
                :
                null
            }
            <div>
            <button type="button" class="btn btn-danger" onClick={() => {this.props.onDeleteSubj(this.props.sbj)}}>Sterge materie</button>
            </div>
            </div>
    }
}

export default ShowProjects;

{/* <button type="button" class="btn btn-danger" onClick={() => this.props.onDeleteSubj(this.props.data[0].subjectId)}>Sterge materie</button> */}
