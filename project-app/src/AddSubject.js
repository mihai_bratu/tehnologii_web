import React, { Component }  from 'react';

class AddSubject extends Component{
    constructor(props){
        super(props);

        this.state = {
            name : '',
            long_name : '',
            description : ''
        }

        this.handleChange = (event) => {
            this.setState({
                [event.target.name] : event.target.value
            })
        }
    }

    updateParent(prop, value){
        this.props.update(prop, value);
    }

    render(){
        return <div class="content">
            <h2>Adaugare materie</h2>
            <p>
            <label>Nume materie (acronim): </label>
            </p>
            <input type="text" size="50" placeholder="acronim" onChange={this.handleChange} name="name"></input>
            <p>
            <label>Nume materie (complet)</label>
            </p>
            <input type="text" size="50" placeholder="nume complet" onChange={this.handleChange} name="long_name"></input>
            <p>
            <label>Descriere</label>
            </p>
            <textarea rows="4" cols="50" placeholder="Descriere" onChange={this.handleChange} name="description"></textarea>
            <p>
            <button type="button" class="btn btn-primary" onClick={() => this.props.onAdd({
                name : this.state.name,
                long_name : this.state.long_name,
                description : this.state.description
            })}>Adauga materie</button>
            </p>
        </div>
    }
}

export default AddSubject;