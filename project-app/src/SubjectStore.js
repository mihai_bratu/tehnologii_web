import {EventEmitter} from 'fbemitter';

const SERVER = 'http://localhost:8080'

class SubjectStore {
    constructor(){
        this.subjects = [];
        this.emitter = new EventEmitter();
    }

    async getAll(){
        try{
            let response = await fetch(`${SERVER}/subjects`);
            let data = await response.json();
            this.subjects = data;
            this.emitter.emit("GET_SUBJ_SUCC");

        }
        catch(error){
            console.warn("Error while retrieving data from server: "+error);
            this.emitter.emit("GET_SUBJ_FAIL");
        }
    }

    async addSubj(subject){
        try{
            await fetch(`${SERVER}/subjects`, {
                method : "post",
                headers : {
                    "Content-Type" : "application/json"
                },
                body : JSON.stringify(subject)
            })
            this.getAll();
        }
        catch(error){
            console.warn(error);
        }
    }

    async delSubj(subject){
        try{
            await fetch(`${SERVER}/subjects/${subject}`, {
                method : "delete",
                headers : {
                    "Content-Type" : "application/json"
                },
                // body : JSON.stringify(subject)
            })
            this.getAll();
        }
        catch(error){
            console.warn(error);
        }
    }
}

export default SubjectStore;