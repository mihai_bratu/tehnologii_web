import React, { Component } from 'react';
import './App.css';
import AddSubject from './AddSubject';
import SubjectStore from './SubjectStore';
import ProjectStore from './ProjectStore';
import ShowProjects from './ShowProjects';
import { Button} from 'react-bootstrap';
import Collapsible from 'react-collapsible';
import 'bootstrap/dist/css/bootstrap.min.css';



class App extends Component {
  constructor() {
    super();

    this.state = {
      subjects  : [],
      projects : [],
      numSubjects : 0,
      activeSbj : null,
      addingSubj : false
    };
    this.sStore = new SubjectStore();
    this.pStore = new ProjectStore();

    this.add = (subject) => {
      this.sStore.addSubj(subject);

      this.setState({
        addSubj : false
      });
    }

    this.deleteSubj = (subject) => {
      
      this.sStore.delSubj(subject);

      this.setState({
        addSubj : false
      });

      console.log("de sters: \n"+subject+"\nsuccess");
    }

  }

  addingSubj() {
    this.setState({
      addingSubj : true,
    })
  };

  openSubject(subjID){
    this.setState({
      addingSubj : false,
      activeSbj : subjID
    });

    this.pStore.getAll(subjID);
    this.pStore.emitter.addListener("GET_PRJ_SUCC", () => {
        this.setState({
            projects : this.pStore.projects
        });
    });
  }

  componentDidMount(){
    this.sStore.getAll();
    this.sStore.emitter.addListener("GET_SUBJ_SUCC", () => {
        this.setState({
            subjects : this.sStore.subjects,
            numSubjects : this.sStore.subjects.length
        })
    })
  }

  

  // addSubj() {
  //   this.setState({
  //     addingSubj : true
  //   })
  // };


  render(){
    return <div class="wrapper">
     <div class="header">
       Software projects tracker
     </div> 
     <div class="left">
         <button type="button" class="btn btn-info btn-block" block onClick={() => {this.addingSubj()}}>Adauga materie</button>
         {
           this.state.subjects.map((subj, index) => <div key={index}>
           <button type="button" class="btn btn-primary btn-block text-left" onClick={() => {this.openSubject(subj.id, this.openSubject)}}>{index+1}. {subj.name}</button>
         </div>)
         }
         </div>
         {
         this.state.addingSubj ? 
          <AddSubject class="content" onAdd={this.add}/>
         : 
         null
         }
        <ShowProjects class="content" prj={this.state.projects} sbj={this.state.activeSbj} onDeleteSubj={this.deleteSubj}/> 
     </div>
   }
  }

export default App;


    // return <div>
    //   <Subjects name="POO"/>
    // </div>
